﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for Agilent P-Series Power Meter (models N1911A and N1912A).</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)[!!!*Q(C=\&gt;1R5O.!%)8B"\5"K7^!_1I&gt;&lt;OIL/(&lt;72U$"*I4/.OYL/#65M"@I+`A+TB=#\[^71RGIMJ/F#KK10-:[-_LZ."+3;LO2@OBY;@N^&gt;JP\6:_8.B`LJ,_/^"TL88^&amp;=^?L1O`L0`^Y=`\Z_=`Z&gt;8&lt;\\P`O`^L^&amp;T&lt;'X&amp;]=^-'&lt;&gt;&amp;637N+#ZD3LKTMG?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:(84CZSE9O=65EG4S:+CC9&amp;EM&amp;1F(QFHM34?")0BUI]C3@R**\%QR!FHM34?"*0YO%U*:\%EXA34_+B6%GS&gt;H)]C9@S#DS"*`!%HM$$F!I]!3#9,#A=&amp;)'BI$0Y%8A#4_$BJQ*0Y!E]A3@QU+X!%XA#4_!*0*R3KR+FG8:S0*32YX%]DM@R/"Z+S`%Y(M@D?"Q0U]HR/"Y(Y5TI&amp;)=AZS2HA(0A?"Q0@_2Y()`D=4S/B[[[1VYL-WGGH2S0Y4%]BM@Q'"Z+S0!9(M.D?!Q0:76Y$)`B-4S'B[FE?!S0Y4%ARK2-,[/9=;)RS!A-$Z^[7KTO5J4%;O^`T@F"64_![A&gt;,`=#I(Q4V$6&lt;@/05.53_U?A(6#[/_907&amp;K)(KC&gt;5&amp;V1.VY(N0'WE\WJ9WU.;U&amp;7V*7USH`O?"B]."_`V?YTBKN^NJO^VK'!;NVWON6CMNFUMN&amp;IO8N^5N_\R&gt;H&lt;[8HI:@G\`_M(G]_`HH;2AXU`'5XT0OI^L]8PI-\U:&gt;[XDSGG?.`A%SXW:+!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.1.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>

		<Item Name="Action-Status" Type="Folder">
			<Item Name="Clear Fail Counter.vi" Type="VI" URL="../Public/Action-Status/Clear Fail Counter.vi"/>
			<Item Name="Clear Peak Hold.vi" Type="VI" URL="../Public/Action-Status/Clear Peak Hold.vi"/>
			<Item Name="Zero or Calibrate.vi" Type="VI" URL="../Public/Action-Status/Zero or Calibrate.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">

			<Item Name="Channel" Type="Folder">
				<Item Name="Configure Auto Gate.vi" Type="VI" URL="../Public/Configure/Channel/Configure Auto Gate.vi"/>
				<Item Name="Configure Auto Scale.vi" Type="VI" URL="../Public/Configure/Channel/Configure Auto Scale.vi"/>
				<Item Name="Configure Calibration Factor.vi" Type="VI" URL="../Public/Configure/Channel/Configure Calibration Factor.vi"/>
				<Item Name="Configure Duty Cycle Correction.vi" Type="VI" URL="../Public/Configure/Channel/Configure Duty Cycle Correction.vi"/>
				<Item Name="Configure Gate Control.vi" Type="VI" URL="../Public/Configure/Channel/Configure Gate Control.vi"/>

				<Item Name="Configure Gates.vi" Type="VI" URL="../Public/Configure/Channel/Configure Gates.vi"/>
				<Item Name="Configure Measurement Speed.vi" Type="VI" URL="../Public/Configure/Channel/Configure Measurement Speed.vi"/>
				<Item Name="Configure Sensor Mode.vi" Type="VI" URL="../Public/Configure/Channel/Configure Sensor Mode.vi"/>
				<Item Name="Configure Sensor Range.vi" Type="VI" URL="../Public/Configure/Channel/Configure Sensor Range.vi"/>
				<Item Name="Configure Trace.vi" Type="VI" URL="../Public/Configure/Channel/Configure Trace.vi"/>
				<Item Name="Configure Video Averaging.vi" Type="VI" URL="../Public/Configure/Channel/Configure Video Averaging.vi"/>
				<Item Name="Configure Video Bandwidth.vi" Type="VI" URL="../Public/Configure/Channel/Configure Video Bandwidth.vi"/>
			</Item>
			<Item Name="System" Type="Folder">
				<Item Name="Clear Frequency Dependent Offset Table.vi" Type="VI" URL="../Public/Configure/System/Clear Frequency Dependent Offset Table.vi"/>
				<Item Name="Clear Sensor Calibration Table.vi" Type="VI" URL="../Public/Configure/System/Clear Sensor Calibration Table.vi"/>
				<Item Name="Configure Linearity Correction Type.vi" Type="VI" URL="../Public/Configure/System/Configure Linearity Correction Type.vi"/>
				<Item Name="Configure Reference Level.vi" Type="VI" URL="../Public/Configure/System/Configure Reference Level.vi"/>
				<Item Name="Configure Reference Oscillator.vi" Type="VI" URL="../Public/Configure/System/Configure Reference Oscillator.vi"/>

				<Item Name="Edit Frequency Dependent Offset Table.vi" Type="VI" URL="../Public/Configure/System/Edit Frequency Dependent Offset Table.vi"/>
				<Item Name="Edit Sensor Calibration Table.vi" Type="VI" URL="../Public/Configure/System/Edit Sensor Calibration Table.vi"/>
				<Item Name="Enable Frequency Dependent Offset Table.vi" Type="VI" URL="../Public/Configure/System/Enable Frequency Dependent Offset Table.vi"/>
				<Item Name="Enable Sensor Calibration Table.vi" Type="VI" URL="../Public/Configure/System/Enable Sensor Calibration Table.vi"/>
				<Item Name="Rename Frequency Dependent Offset Table.vi" Type="VI" URL="../Public/Configure/System/Rename Frequency Dependent Offset Table.vi"/>
				<Item Name="Rename Sensor Calibration Table.vi" Type="VI" URL="../Public/Configure/System/Rename Sensor Calibration Table.vi"/>
			</Item>
			<Item Name="Configure Channel Offset.vi" Type="VI" URL="../Public/Configure/Configure Channel Offset.vi"/>
			<Item Name="Configure Correction Frequency.vi" Type="VI" URL="../Public/Configure/Configure Correction Frequency.vi"/>
			<Item Name="Configure Display Offset.vi" Type="VI" URL="../Public/Configure/Configure Display Offset.vi"/>
			<Item Name="Configure Measurement Averaging.vi" Type="VI" URL="../Public/Configure/Configure Measurement Averaging.vi"/>
			<Item Name="Configure Measurement Limits.vi" Type="VI" URL="../Public/Configure/Configure Measurement Limits.vi"/>

			<Item Name="Configure Measurement.vi" Type="VI" URL="../Public/Configure/Configure Measurement.vi"/>
			<Item Name="Configure Preset.vi" Type="VI" URL="../Public/Configure/Configure Preset.vi"/>
			<Item Name="Configure Recorder Output.vi" Type="VI" URL="../Public/Configure/Configure Recorder Output.vi"/>
			<Item Name="Configure Relative Measurement.vi" Type="VI" URL="../Public/Configure/Configure Relative Measurement.vi"/>
			<Item Name="Configure Trigger Characteristics.vi" Type="VI" URL="../Public/Configure/Configure Trigger Characteristics.vi"/>
			<Item Name="Configure Trigger.vi" Type="VI" URL="../Public/Configure/Configure Trigger.vi"/>
			<Item Name="Configure Zeroing and Calibrating.vi" Type="VI" URL="../Public/Configure/Configure Zeroing and Calibrating.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Low Level" Type="Folder">
				<Item Name="Abort.vi" Type="VI" URL="../Public/Data/Low Level/Abort.vi"/>
				<Item Name="Fetch Duty Cycle.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Duty Cycle.vi"/>
				<Item Name="Fetch Limit Test Result.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Limit Test Result.vi"/>
				<Item Name="Fetch Measurement.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Measurement.vi"/>
				<Item Name="Fetch Pulse Data.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Pulse Data.vi"/>

				<Item Name="Fetch Trace.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Trace.vi"/>
				<Item Name="Fetch Transition Data.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Transition Data.vi"/>
				<Item Name="Initiate.vi" Type="VI" URL="../Public/Data/Low Level/Initiate.vi"/>
				<Item Name="Send Software Trigger.vi" Type="VI" URL="../Public/Data/Low Level/Send Software Trigger.vi"/>
				<Item Name="Wait for Operation Complete.vi" Type="VI" URL="../Public/Data/Low Level/Wait for Operation Complete.vi"/>
			</Item>
			<Item Name="Read Duty Cycle.vi" Type="VI" URL="../Public/Data/Read Duty Cycle.vi"/>
			<Item Name="Read Limit Test Result.vi" Type="VI" URL="../Public/Data/Read Limit Test Result.vi"/>
			<Item Name="Read Measurement.vi" Type="VI" URL="../Public/Data/Read Measurement.vi"/>
			<Item Name="Read Pulse Data.vi" Type="VI" URL="../Public/Data/Read Pulse Data.vi"/>
			<Item Name="Read Trace.vi" Type="VI" URL="../Public/Data/Read Trace.vi"/>

			<Item Name="Read Transition Data.vi" Type="VI" URL="../Public/Data/Read Transition Data.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
		</Item>

		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">

		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
		<Item Name="Query Trigger Source.vi" Type="VI" URL="../Private/Query Trigger Source.vi"/>
	</Item>
	<Item Name="Agilent N191X Series Readme.html" Type="Document" URL="../Agilent N191X Series Readme.html"/>
</Library>
